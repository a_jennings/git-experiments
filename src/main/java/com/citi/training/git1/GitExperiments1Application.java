package com.citi.training.git1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitExperiments1Application {

	public static void main(String[] args) {
		SpringApplication.run(GitExperiments1Application.class, args);
	}

}
